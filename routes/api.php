<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/', function (Request $request) {

    $client_id = $request->input('id');
    $client_secret = $request->input('secret');
    request()->request->add([
        'grant_type' => 'client_credentials',
        'client_id' => $client_id,
        'client_secret' => $client_secret,
        'scope' => [
            'dados.tirar',
            'usuarios.login',
            'usuarios.jugadores.get',
            'usuarios.jugadores.post',
            'usuarios.jugadores.put',
            'juegos.generar',
            'juegos.simular',
            'torneos.partida.get',
            'torneos.partida.put'
        ],
    ]);
    $res = Route::dispatch(Request::create('oauth/token', 'POST'));
    $data = json_decode($res->getContent());
    $isOk = $res->getStatusCode() === 200;
    if ($isOk) {
        return response()->json([
            'jwt' => $data->access_token
        ], 201);
    } else {
        return response()->json([
            'errors' => $data
        ], 400);
    }
    // return response()->json([
    //     'data' => $isOk ? ['jwt' => $data] : null,
    //     'errors' => $isOk ? null : [$data]
    // ], 200);
});

// Route::post('token', function (Request $request) {
//     $oauth_client_id = 4;
//     $oauth_client = 'xnytr48CfFq8P1Yvgsig7fFlbL9sqmMsb2H1mS9H';

//     $body = [
//         'client_id' => $oauth_client_id,
//         'client_secret' => $oauth_client,
//         'grant_type' => 'client_credentials',
//         'scope' => '*'
//     ];
//     $this->json('POST', '/oauth/token', $body, ['Accept' => 'application/json'])
//         ->assertStatus(200)
//         ->assertJsonStructure(['token_type', 'expires_in', 'access_token', 'refresh_token']);
// });
