<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();
        Passport::tokensCan([
            'dados.tirar' => 'Tirar dados',
            'usuarios.login' => 'Iniciar sesión',
            'usuarios.jugadores.get' => 'Obtener usuarios',
            'usuarios.jugadores.post' => 'Crear usuarios',
            'usuarios.jugadores.put' => 'Actualizar usuarios',
            'juegos.generar' => 'Nuevo juego',
            'juegos.simular' => 'Simular juego',
            'torneos.partida.get' => 'Obtener partidas',
            'torneos.partida.put' => 'Actualizar partidas',
        ]);
    }
}
